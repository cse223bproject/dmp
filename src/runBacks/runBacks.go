package main

import (
	"local/store"
	"mchord"
)

func main() {

	ready := make(chan bool)
	n1 := mchord.Node{
		Addr:  "100.90.243.35:12347",
		Store: store.NewStorage(),
		Ready: ready,
		R:     1,
	}
	n1.CreateChordRing()
	ready = make(chan bool)
	<-ready
}
