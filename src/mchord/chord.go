package mchord

import (
	"encoding/json"
	"fmt"
	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/sdl_mixer"
	"hash/fnv"
	"local"
	"log"
	"net"
	"net/rpc"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

const MaxUint = ^uint32(0)
const MaxInt = int32(MaxUint >> 1)

type LocalPlayer struct{}

type PlayerInterface interface {
	Play(string, *bool) error
}

type Container struct {
	Base string
	Name string
	Seek int64
	Time time.Time
	Meta string
}

type ChordRing interface {
	CreateNode() error
	CreateChordRing() error
	JoinChordRing(string) error
	Lookup(string, string) string
}
type Node struct {
	Addr  string        // listen address
	Store local.Storage // the underlying storage it should use
	Ready chan bool     // send a value when server is ready
	R     int
}

func (self *LocalPlayer) Play(StrEncCont string, succ *bool) error {
	//go func() error {
	fmt.Println("Entering Play", time.Now())
	var cont Container
	//fmt.Println("In Play ", StrEncCont)
	json.Unmarshal([]byte(StrEncCont), &cont)
	base := Node{
		Addr: cont.Base,
	}
	//fmt.Println(base.Addr, "address")
	uname := "saienthan"
	//fmt.Println("Container is ", cont)
	splits := strings.Split(cont.Meta, ",")
	songLength, _ := strconv.Atoi(strings.TrimSpace(splits[0]))
	num, _ := strconv.Atoi(splits[1])
	//fmt.Println(num)
	ext := splits[2]
	destBase := filepath.Join(os.Getenv("DMPHOME"), strings.Split(base.Addr, ":")[1])
	//fmt.Println("IN PLAY", filepath.Join(os.Getenv("DMPHOME"), strings.Split(base.Addr, ":")[1]))
	if err := sdl.Init(sdl.INIT_AUDIO); err != nil {
		log.Println(err)
		return nil
	}

	if err := mix.Init(mix.INIT_MP3); err != nil {
		log.Println(err)
		return nil
	}

	if err := mix.OpenAudio(22050, mix.DEFAULT_FORMAT, 2, 4096); err != nil {
		log.Println(err)
		return nil
	}
	combined := filepath.Join(destBase, cont.Name) + ext
	//fmt.Println("Combined path and file to play is", combined)
	destSplit := filepath.Join(destBase, "splits", cont.Name+fmt.Sprintf("%03d", 0))
	source := base.Lookup(base.Addr, destSplit)
	sourceBase := filepath.Join(os.Getenv("DMPHOME"), strings.Split(source, ":")[1])
	fmt.Println(sourceBase, os.Getenv("DMPHOME"))
	cmd := exec.Command("sshpass", "-p", "saienthan", "scp", uname+"@"+strings.Split(source, ":")[0]+":"+filepath.Join(sourceBase, "splits", cont.Name)+fmt.Sprintf("%03d", 0), combined)
	fmt.Println("sshpass", "-p", "saienthan", "scp", uname+"@"+strings.Split(source, ":")[0]+":"+filepath.Join(sourceBase, "splits", cont.Name)+fmt.Sprintf("%03d", 0), combined)
	_ = cmd.Start()
	e := cmd.Wait()
	fmt.Println("Downloaded file 1", time.Now())
	if e != nil {
		fmt.Println("Error here", e)
		return nil
	}
	music, err := mix.LoadMUS(combined)
	if err != nil {
		fmt.Println("ERROR1 ", err)
	} else {
		ticker := updateTicker(cont.Time)

		<-ticker.C
		err := music.Play(1)
		if err != nil {
			fmt.Println("ERROR2 ", err)
		} else {
			fmt.Println("Basename", combined)

			for i := 1; i < num; i++ {
				//time.Sleep(time.Millisecond * 15000)
				destSplit = filepath.Join(destBase, "splits", cont.Name+fmt.Sprintf("%03d", i))
				source = base.Lookup(base.Addr, destSplit)
				sourceBase = filepath.Join(os.Getenv("DMPHOME"), strings.Split(source, ":")[1])
				//fmt.Println("Filename" + filename)
				//fmt.Println("sshpass", "-p", "saienthan", "scp", uname+"@"+strings.Split(dest, ":")[0]+":"+filename, filename)
				//go func(dest string, filename string) {
				cmd = exec.Command("sshpass", "-p", "saienthan", "scp", uname+"@"+strings.Split(source, ":")[0]+":"+filepath.Join(sourceBase, "splits", cont.Name)+fmt.Sprintf("%03d", i), destSplit)
				fmt.Println("Executing ", "sshpass", "-p", "saienthan", "scp", uname+"@"+strings.Split(source, ":")[0]+":"+filepath.Join(sourceBase, "splits", cont.Name)+fmt.Sprintf("%03d", i), destSplit)
				_ = cmd.Start()
				e = cmd.Wait()
				if e != nil {
					fmt.Println("ERROR here sdfasdf", e)
					return nil
				}
				//}(dest, filename)
				//ffmpeg -i "concat:output001.mp3|output002.mp3" -c copy output.mp3
				//cmd := exec.Command("sh", "-c", "ffmpeg -i \"concat:"+basename+"|"+filename+"\" -c copy temp.mp3 -y")
				//fmt.Println("sh", "-c", "ffmpeg -i \"concat:"+basename+"|"+filename+"\" -c copy temp.mp3 -y")
				cmd = exec.Command("sh", "-c", "cat "+destSplit+" >> "+combined)
				fmt.Println("Concatenating", "sh", "-c", "cat "+destSplit+" >> "+combined)
				_ = cmd.Start()
				e = cmd.Wait()
				//fmt.Println("ffmpeg", "-i", "\"concat:"+basename+"|"+filename+"\"", "-c", "copy", "temp.mp3", "-y")
				if e != nil {
					fmt.Println("ERROR IN PLAY", e)
					//return e
				}

				/*
					//cmd = exec.Command("mv", "temp.mp3", basename)
					//_ = cmd.Start()
					//e = cmd.Wait()
					if e != nil {
						//fmt.Println(e)
						return e
					}
				*/

			}
		}

		num_splits := songLength / 1
		for i := 0; i < num_splits; i++ {
			sdl.Delay(uint32(1 * 1000))
			if !mix.PlayingMusic() {
				mix.SetMusicPosition(int64((i + 1) * 1))
			}
		}
		sdl.Delay(uint32(songLength % 10 * 1000))
		music.Free()
		*succ = true
		defer sdl.Quit()
		defer mix.Quit()
		defer mix.CloseAudio()
	}
	return nil
	//}()
	//return nil
}

func NewClient(addr string) local.Storage {
	// connect to the server
	return &client{addr: addr}
}

func (self *Node) CheckPredecessor() error {
	var pred string
	var succ bool
	var kv local.KeyValue
	conn, _ := rpc.Dial("tcp", self.Addr)
	_ = conn.Call("Storage.Get", "predecessor", &pred)
	conn.Close()
	if pred == "Nil" {
		time.Sleep(time.Millisecond * 100)
		self.CheckPredecessor()
	}

	conn, e1 := rpc.Dial("tcp", pred)
	if e1 != nil {
		kv.Key = "predecessor"
		kv.Value = "Nil"
		conn, _ = rpc.Dial("tcp", self.Addr)
		_ = conn.Call("Storage.Set", &kv, &succ)
		conn.Close()
	}
	e2 := conn.Call("Storage.Get", "predecessor", &pred)
	conn.Close()
	if e2 != nil {
		kv.Key = "predecessor"
		kv.Value = "Nil"
		conn, _ = rpc.Dial("tcp", self.Addr)
		_ = conn.Call("Storage.Set", &kv, &succ)
		conn.Close()
	}
	time.Sleep(time.Millisecond * 100)
	self.CheckPredecessor()
	return nil
}
func (self *Node) CreateChordRing() error {
	//fmt.Println("here")
	var kv local.KeyValue
	var target string
	var succ bool
	var dummy bool
	var succl local.List
	//fmt.Println("Before create node")
	go self.CreateNode()
	ready := <-self.Ready
	if ready == false {
		return fmt.Errorf("Create node failed")
	}
	//fmt.Println("After create node", ready)

	//fmt.Println("BEFORE SUCCESSORS")
	conn, e := rpc.Dial("tcp", self.Addr)
	for i := 0; i < self.R; i++ {
		succl.L = append(succl.L, self.Addr)
	}
	succl.L = append([]string{"successors"}, succl.L...)
	e = conn.Call("Storage.ListSet", &succl, &dummy)
	if e != nil {
		conn.Close()
		//fmt.Println("ERROR", e)
		return e
	}
	conn.Close()

	conn, e = rpc.Dial("tcp", self.Addr)
	if e != nil {
		conn.Close()
		return e
	}
	kv.Key = "predecessor"
	kv.Value = "Nil"
	e = conn.Call("Storage.Set", &kv, &succ)
	if e != nil {
		conn.Close()
		return e
	}
	conn.Close()

	//fmt.Println("BEFORE ALLUSERS")
	kv.Key = "AllUsers"
	kv.Value = self.Addr
	target = self.Lookup(self.Addr, kv.Key)
	conn, e = rpc.Dial("tcp", target)
	if e != nil {
		conn.Close()
		return e
	}
	e = conn.Call("Storage.ListAppend", &kv, &succ)
	if e != nil {
		conn.Close()
		return e
	}
	//fmt.Println("END OF CREATE CHORD RING")
	conn.Close()
	go self.Stabilize()
	go self.CheckPredecessor()
	return nil
}
func (self *Node) CreateNode() error {
	var lplay PlayerInterface = new(LocalPlayer)
	rpcServer := rpc.NewServer()
	rpcServer.Register(self.Store)
	rpcServer.Register(lplay)
	l, e := net.Listen("tcp", self.Addr)
	if e != nil {
		self.Ready <- false

	}
	if self.Ready != nil {
		self.Ready <- true
	}
	rpcServer.Accept(l)
	return nil
}

func (self *Node) JoinChordRing(knownAddr string) error {
	var kv local.KeyValue
	var target string
	var succl local.List
	var succ string
	var dummy bool
	//fmt.Println("Before calling find successor")
	go self.CreateNode()
	ready := <-self.Ready
	if ready == false {
		return fmt.Errorf("Create node failed")
	}
	//fmt.Println("Calling find successor")
	kv.Key = "successors"
	succ = self.Lookup(knownAddr, self.Addr)
	conn, e := rpc.Dial("tcp", succ)
	if e != nil {
		conn.Close()
		return e
	}
	//e = conn.Call("Storage.ListAppend", &kv, &success)
	e = conn.Call("Storage.ListGet", "successors", &succl)
	if e != nil {
		conn.Close()
		return e
	}
	conn.Close()
	succl.L = succl.L[:len(succl.L)-1]
	succl.L = append([]string{succ}, succl.L...)
	succl.L = append([]string{"successors"}, succl.L...)
	conn, e = rpc.Dial("tcp", self.Addr)
	e = conn.Call("Storage.ListSet", &succl, &dummy)
	if e != nil {
		conn.Close()
		return e
	}
	conn.Close()

	conn, e = rpc.Dial("tcp", self.Addr)
	if e != nil {
		conn.Close()
		//fmt.Println("error here")
		return e
	}
	kv.Key = "predecessor"
	kv.Value = "Nil"
	e = conn.Call("Storage.Set", &kv, &dummy)
	if e != nil {
		conn.Close()
		//fmt.Println("error here1", e)
		return e
	}
	conn.Close()

	//fmt.Println("END OF JOIN CHORD RING")
	kv.Key = "AllUsers"
	kv.Value = self.Addr
	target = self.Lookup(self.Addr, kv.Key)
	conn, e = rpc.Dial("tcp", target)
	if e != nil {
		conn.Close()
		//fmt.Println("Join chord ring", target, e)
		return e
	}
	e = conn.Call("Storage.ListAppend", &kv, &dummy)
	if e != nil {
		conn.Close()
		//fmt.Println("Join chord ring", target, e)
		return e
	}
	conn.Close()
	go self.Stabilize()
	go self.CheckPredecessor()
	return nil

	//*********** knownAddr  --------- b.Addr ------------ knownAddr.SAddr[0] *************

}

func Hash(addr string) int {
	h := fnv.New32a()
	h.Write([]byte(addr))
	return int(h.Sum32())
}

func (self *Node) Lookup(knownAddr string, lookupVal string) string {
	//fmt.Println(knownAddr)
	var succl local.List
	//fmt.Println("Lookup")
	conn, e := rpc.Dial("tcp", knownAddr)
	//fmt.Println("In lookup", e)
	if e != nil {
		fmt.Println("Error in lookup ", e)
	}
	_ = conn.Call("Storage.ListGet", "successors", &succl)
	//fmt.Println("Lookup ListGet", e, succl.L)
	conn.Close()
	for Hash(knownAddr) > Hash(lookupVal) || Hash(lookupVal) >= Hash(self.getFirstAlive(succl.L)) {
		//fmt.Println(Hash(lookupVal), Hash(knownAddr), knownAddr, succl)
		if knownAddr == self.getFirstAlive(succl.L) { //When chord ring has only one node in it
			break

		}
		if Hash(knownAddr) > Hash(self.getFirstAlive(succl.L)) && (Hash(lookupVal) > Hash(knownAddr) || Hash(lookupVal) <= Hash(self.getFirstAlive(succl.L))) {
			break
		}
		knownAddr = self.getFirstAlive(succl.L)
		conn, e = rpc.Dial("tcp", knownAddr)
		if e != nil {
			break
		}
		_ = conn.Call("Storage.ListGet", "successors", &succl)
		conn.Close()
	}
	return self.getFirstAlive(succl.L)
}

func (self *Node) getFirstAlive(list []string) string {
	var dummy string
	//fmt.Println("In GetFirstAlive", list)
	for _, user := range list {
		conn, e1 := rpc.Dial("tcp", user)
		if e1 != nil {
			continue
		}
		e2 := conn.Call("Storage.Get", "predecessor", &dummy)
		if e2 != nil {
			conn.Close()
			continue
			fmt.Println("ERROR in GetfirstAlive", e2)
		}
		conn.Close()
		if e1 == nil && e2 == nil {
			//fmt.Println("Succ GFA", user)
			return user
		}
	}
	//fmt.Println("GFA Returning self")
	return self.Addr
}

func InRange(c int, s int, p int) bool {
	//fmt.Println("s", s, "c", c, "p", p)
	if s < p {
		if c >= s && c < p {
			return true
		} else {
			return false
		}
	} else if s > p {
		if !(c >= p && c < s) {
			return true
		} else {
			return false
		}

	} else {
		return true
	}
}
func (self *Node) Stabilize() {
	//fmt.Println("IN STABILIZE")
	start := time.Now()
	var succl local.List
	var keylist local.List
	var ltoc local.List
	var succ string
	var s_pred string
	var pred string
	var res string
	var curr = self.Addr
	var dummy bool
	var file string
	var src string
	var kv local.KeyValue
	base := os.Getenv("DMPHOME")
	uname := os.Getenv("DMPUSER")
	p := &local.Pattern{Prefix: "", Suffix: ""}
	conn, _ := rpc.Dial("tcp", curr)
	//fmt.Println(e)
	_ = conn.Call("Storage.ListGet", "successors", &succl)
	conn.Close()

	succ = self.getFirstAlive(succl.L)
	hashed_succ := Hash(succ)

	conn, e := rpc.Dial("tcp", succ)
	if e != nil {
		self.Stabilize()
	}
	_ = conn.Call("Storage.ListGet", "successors", &succl)
	conn.Close()

	conn, e = rpc.Dial("tcp", succ)
	if e != nil {
		self.Stabilize()
	}
	_ = conn.Call("Storage.Get", "predecessor", &s_pred)
	conn.Close()
	conn, e = rpc.Dial("tcp", curr)
	if e != nil {
		fmt.Println("error", e)
	}
	e = conn.Call("Storage.Get", "predecessor", &pred)
	if e != nil {
		fmt.Println("error", e)
	}
	conn.Close()
	//remove me
	conn, e = rpc.Dial("tcp", curr)
	if e != nil {
		fmt.Println("error", e)
	}
	e = conn.Call("Storage.ListGet", "successors", &succl)
	if e != nil {
		fmt.Println("error", e)
	}
	conn.Close()

	//till here

	//fmt.Println("Node:", self.Addr, "Succ:", succl.L, "Pred", pred, "Successor Pred", s_pred)
	//fmt.Println("Before", succl.L)
	conn, e = rpc.Dial("tcp", succ)
	if e != nil {
		self.Stabilize()
	}
	_ = conn.Call("Storage.ListGet", "successors", &succl)
	conn.Close()
	succl.L = succl.L[:len(succl.L)-1]
	if s_pred != "Nil" && InRange(Hash(curr), hashed_succ, Hash(s_pred)) {
		succl.L = append([]string{s_pred}, succl.L...)
	} else {
		succl.L = append([]string{succ}, succl.L...)
	}
	//fmt.Println("After", succl.L)
	conn, _ = rpc.Dial("tcp", curr)
	succl.L = append([]string{"successors"}, succl.L...)
	e = conn.Call("Storage.ListSet", &succl, &dummy)
	if e != nil {
		fmt.Println("error in Stabilize", e)
	}
	conn.Close()
	conn, e = rpc.Dial("tcp", curr)
	if e != nil {
		fmt.Println("error", e)
	}
	e = conn.Call("Storage.Keys", p, &keylist)
	if e != nil {
		fmt.Println("error", e)
	}
	conn.Close()
	//cmd := exec.Command("sshpass", "-p", "saienthan", "scp", file, uname+"@"+strings.Split(dest, ":")[0]+":"+file)
	//fmt.Println("KEYLIST ", curr, keylist.L)
	for _, key := range keylist.L {
		if key == "predecessor" {
			continue
		}
		//fmt.Println("HERE")
		conn, e = rpc.Dial("tcp", curr)
		if e != nil {
			fmt.Println("error", e)
		}
		e = conn.Call("Storage.Get", key, &res)
		if e != nil {
			fmt.Println("error", e)
		}
		conn.Close()
		kv.Key = key
		kv.Value = res
		for _, succ := range succl.L {
			if succ == "successors" {
				continue
			}
			conn, e = rpc.Dial("tcp", succ)
			if e != nil {
				self.Stabilize()
			}
			e = conn.Call("Storage.Set", &kv, &dummy)
			conn.Close()
		}
		//fmt.Println(res)
		//go func(key string, res string) {
		splits := strings.Split(res, ",")
		num, _ := strconv.Atoi(splits[1])
		//ext := splits[2]
		for i := 0; i < num; i++ {
			file = key + fmt.Sprintf("%03d", i)
			src = filepath.Join(base, strings.Split(curr, ":")[1], "splits", file)
			//fmt.Println("SUCCs SUCC", succl.L)
			//fmt.Println(self.Addr, file, curr)
			//fmt.Println(self.Lookup(self.Addr, file))
			if self.Lookup(self.Addr, file) == curr {
				//fmt.Println("Inside go routine")
				for _, succ = range succl.L {
					if succ == "successors" {
						continue
					}
					//fmt.Println("SUCC IS", succ)
					cmd := exec.Command("sshpass", "-p", "saienthan", "scp", src, uname+"@"+strings.Split(succ, ":")[0]+":"+filepath.Join(base, strings.Split(succ, ":")[1], "splits", file))
					fmt.Println("COPYING IN STABILIZE", "sshpass", "-p", "saienthan", "scp", src, uname+"@"+strings.Split(succ, ":")[0]+":"+filepath.Join(base, strings.Split(succ, ":")[1], "splits", file))
					e := cmd.Start()
					if e != nil {
						fmt.Println("ERROR3 ", e)
					}
					e = cmd.Wait()
					if e != nil {
						fmt.Println("ERROR4 ", e)
					}
				}
			}

		}
		//}(key, res)
	}

	conn, e = rpc.Dial("tcp", curr)
	e = conn.Call("Storage.ListKeys", p, &keylist)
	conn.Close()
	for _, key := range keylist.L {
		if key == "successors" {
			continue
		}
		conn, e = rpc.Dial("tcp", curr)
		e = conn.Call("Storage.ListGet", key, &ltoc)
		ltoc.L = append([]string{"AllUsers"}, ltoc.L...)
		conn.Close()
		for _, succ := range succl.L {
			if succ == "successors" {
				continue
			}
			conn, e = rpc.Dial("tcp", succ)
			if e != nil {
				continue
			}
			e = conn.Call("Storage.ListSet", &ltoc, &dummy)
			conn.Close()
		}
	}

	//fmt.Println("FIND ME !!!")
	Notify(succ, self.Addr)
	fmt.Println(time.Since(start))
	time.Sleep(time.Millisecond * 100)
	self.Stabilize()
}

func Notify(succ string, curr string) {
	var s_pred string
	var success bool
	var kv local.KeyValue
	//fmt.Println("IN NOTIFY")
	conn, e := rpc.Dial("tcp", succ)
	if e != nil {
		return
	}

	_ = conn.Call("Storage.Get", "predecessor", &s_pred)
	conn.Close()
	hashed_succ := Hash(succ)
	hashed_s_pred := Hash(s_pred)
	hashed_curr := Hash(curr)
	//fmt.Println(succ, hashed_succ)
	//fmt.Println(s_pred, hashed_s_pred)
	//fmt.Println(curr, hashed_curr)
	//fmt.Println("INRANGE", InRange(hashed_s_pred, hashed_succ, hashed_curr))
	if s_pred == "Nil" || InRange(hashed_s_pred, hashed_succ, hashed_curr) {
		conn, e = rpc.Dial("tcp", succ)
		if e != nil {
			return
		}
		kv.Key = "predecessor"
		kv.Value = curr
		_ = conn.Call("Storage.Set", &kv, &success)
		conn.Close()
	}
}

func updateTicker(playTime time.Time) *time.Ticker {
	fmt.Println(playTime, "- next tick")
	diff := playTime.Sub(time.Now())
	return time.NewTicker(diff)
}
