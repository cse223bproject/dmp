package mchord

import (
	"local"
	"net/rpc"
)

type client struct {
	addr string
}

func (self *client) Get(key string, value *string) error {

	// connect to the server
	conn, e := rpc.Dial("tcp", self.addr)
	if e != nil {
		return e
	}
	// perform the call
	e = conn.Call("Storage.Get", key, value)
	if e != nil {
		conn.Close()
		return e
	}
	return nil
}

func (self *client) Set(kv *local.KeyValue, succ *bool) error {

	// connect to the server
	conn, e := rpc.Dial("tcp", self.addr)
	if e != nil {
		return e
	}
	// perform the call
	e = conn.Call("Storage.Set", kv, succ)
	if e != nil {
		conn.Close()
		return e
	}
	return conn.Close()
}

func (self *client) Keys(p *local.Pattern, list *local.List) error {

	// connect to the server
	conn, e := rpc.Dial("tcp", self.addr)
	if e != nil {
		return e
	}
	// perform the call
	list.L = nil
	e = conn.Call("Storage.Keys", p, list)
	if list.L == nil {
		list.L = []string{}
	}
	if e != nil {
		conn.Close()
		return e
	}

	return conn.Close()
}

func (self *client) ListGet(key string, list *local.List) error {
	// connect to the server
	conn, e := rpc.Dial("tcp", self.addr)
	if e != nil {
		return e
	}

	// perform the call
	list.L = nil
	e = conn.Call("Storage.ListGet", key, list)
	if e != nil {
		conn.Close()
		return e
	}
	if list.L == nil {
		list.L = []string{}
	}
	return conn.Close()
}

func (self *client) ListSet(list *local.List, dummy *bool) error {
	// connect to the server
	conn, e := rpc.Dial("tcp", self.addr)
	if e != nil {
		return e
	}

	// perform the call
	e = conn.Call("Storage.ListSet", list, dummy)
	if e != nil {
		conn.Close()
		return e
	}
	if list.L == nil {
		list.L = []string{}
	}
	return conn.Close()
}
func (self *client) ListAppend(kv *local.KeyValue, succ *bool) error {

	// connect to the server
	conn, e := rpc.Dial("tcp", self.addr)
	if e != nil {
		return e
	}
	// perform the call
	e = conn.Call("Storage.ListAppend", kv, succ)
	if e != nil {
		conn.Close()
		return e
	}
	return conn.Close()
}

func (self *client) ListRemove(kv *local.KeyValue, n *int) error {

	// connect to the server
	conn, e := rpc.Dial("tcp", self.addr)
	if e != nil {
		return e
	}
	// perform the call
	e = conn.Call("Storage.ListRemove", kv, n)
	if e != nil {
		conn.Close()
		return e
	}
	return conn.Close()
}

func (self *client) ListKeys(p *local.Pattern, list *local.List) error {

	// connect to the server
	conn, e := rpc.Dial("tcp", self.addr)
	if e != nil {
		return e
	}
	// perform the call
	list.L = nil
	e = conn.Call("Storage.ListKeys", p, list)
	if e != nil {
		conn.Close()
		return e
	}
	if list.L == nil {
		list.L = []string{}
	}

	return conn.Close()
}

func (self *client) Clock(atLeast uint64, ret *uint64) error {
	// connect to the server
	conn, e := rpc.Dial("tcp", self.addr)
	if e != nil {
		return e
	}
	// perform the call
	e = conn.Call("Storage.Clock", atLeast, ret)
	if e != nil {
		conn.Close()
		return e
	}
	return conn.Close()
}

var _ local.Storage = new(client)
