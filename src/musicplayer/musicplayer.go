package musicplayer

import (
	"bytes"
	"encoding/json"
	"fmt"
	"local"
	"log"
	"mchord"
	"net/rpc"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

type Library struct {
	Base mchord.Node
}
type libraryI interface {
	Upload(string, string)
	QueuePlay(string, int64)
}

func (self *Library) Upload(name string, path string) {
	var dest string
	var succ bool
	var out bytes.Buffer
	uname := "saienthan"

	cmd := exec.Command("sh", "-c", "ffmpeg -i "+path+" 2>&1 | grep \"Duration\"| cut -d ' ' -f 4 | sed s/,// | sed 's@\\..*@@g' | awk '{ split($1, A, \":\"); split(A[3], B, \".\"); print 3600*A[1] + 60*A[2] + B[1] }'")
	fmt.Println("ffmpeg", "-i", path, "grep \"Duration\"| cut -d ' ' -f 4 | sed s/,// | sed 's@\\..*@@g' | awk '{ split($1, A, \":\"); split(A[3], B, \".\"); print 3600*A[1] + 60*A[2] + B[1] }'")
	cmd.Stdout = &out
	err := cmd.Start()
	err = cmd.Wait()
	//fmt.Println(out)
	//fmt.Println("Error here", err)
	songLength := out.String()
	//fmt.Println("Song length", songLength)
	splitsPath := filepath.Join(os.Getenv("DMPHOME"), strings.Split(self.Base.Addr, ":")[1], "splits", name)
	fmt.Println("Splitspath ", splitsPath)
	//cmd := exec.Command("ffmpeg", "-i", path, "-c", "copy", "-map", "0", "-flags", "+global_header", "-segment_time", splitSize, "-f", "segment", splitsPath+"%03d"+filepath.Ext(path))
	cmd = exec.Command("split", "--bytes=100K", "-d", "-a", "3", path, splitsPath)
	fmt.Println("Splitting files :", "split", "--bytes=100K", "-d", "-a", "3", path, splitsPath)
	err = cmd.Start()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Waiting")
	err = cmd.Wait()
	log.Printf("Command finished with error: %v", err)
	files, err := filepath.Glob(splitsPath + "*")
	fmt.Println(files)
	dest = self.Base.Lookup(self.Base.Addr, name)
	fmt.Println("DEST", dest)
	conn, e := rpc.Dial("tcp", dest)
	if e != nil {
		fmt.Println(e)
		os.Exit(0)
	}
	var kv local.KeyValue
	kv.Key = name
	kv.Value = songLength + "," + strconv.Itoa(len(files)) + "," + filepath.Ext(path)
	e = conn.Call("Storage.Set", &kv, &succ)
	conn.Close()
	if e != nil {
		fmt.Println(e)
		os.Exit(1)
	}

	for _, file := range files {
		dest = self.Base.Lookup(self.Base.Addr, filepath.Base(file))
		cmd = exec.Command("sshpass", "-p", "saienthan", "scp", file, uname+"@"+strings.Split(dest, ":")[0]+":"+filepath.Join(os.Getenv("DMPHOME"), strings.Split(dest, ":")[1], "splits", filepath.Base(file)))
		fmt.Println("Executing: ", "sshpass", "-p", "saienthan", "scp", file, uname+"@"+strings.Split(dest, ":")[0]+":"+filepath.Join(os.Getenv("DMPHOME"), strings.Split(dest, ":")[1], "splits", filepath.Base(file)))
		err = cmd.Start()
		if err != nil {
			log.Fatal(err)
		}
		err = cmd.Wait()
		log.Printf("Command finished with error: %v", err)
	}
	// Iterate through files and scp each file to desination
}

func (self *Library) QueuePlay(name string, seek int64) {
	/* Iterate through list of clients and call play on each of them
	 */
	//var strEncCont string
	var succ bool
	var value string
	var userlist local.List
	refNode := mchord.Node{
		Addr: self.Base.Addr,
	}
	target := refNode.Lookup(refNode.Addr, "AllUsers")
	fmt.Println(target)
	conn, e := rpc.Dial("tcp", target)
	if e != nil {
		fmt.Println("ERR ", e)
	}

	e = conn.Call("Storage.ListGet", "AllUsers", &userlist)
	if e != nil {
		fmt.Println("ERR ", e)
	}
	conn.Close()
	fmt.Println("USER LIST", userlist.L)
	//userlist.L = []string{"127.0.0.1:12347", "127.0.0.1:12348", "127.0.0.1:12349"}

	//Lookup for song metadata
	target = refNode.Lookup(refNode.Addr, name)
	fmt.Println("DEST LATER", target)
	conn, e = rpc.Dial("tcp", target)
	if e != nil {
		fmt.Println("ERR ", e)
	}

	e = conn.Call("Storage.Get", name, &value)
	if e != nil {
		fmt.Println("ERR ", e)
	}
	conn.Close()

	cont := mchord.Container{
		Base: target,
		Name: name,
		Seek: seek,
		Time: time.Now().Local().Add(time.Second * time.Duration(3)),
		Meta: value,
	}

	/*
		encCont, _ := json.Marshal(cont)
		strEncCont = string(encCont)
	*/

	nconcur := len(userlist.L)
	done := make(chan bool, nconcur)
	for _, user := range userlist.L {
		go func(user string, cont mchord.Container, done chan<- bool) {
			cont.Base = user
			encCont, _ := json.Marshal(cont)
			strEncCont := string(encCont)
			conn, e := rpc.Dial("tcp", user)
			if e != nil {
				fmt.Println("ERR", e)
			}
			e = conn.Call("LocalPlayer.Play", strEncCont, &succ)
			if e != nil {
				fmt.Println("Error in queue play", e)
				//os.Exit(1)
			}
			conn.Close()
			done <- true
		}(user, cont, done)
	}
	for i := 0; i < nconcur; i++ {
		<-done
	}

}
