package main

import (
	"local/store"
	"mchord"
	"os"
)

func main() {
	//InRange(c,s,p)
	/*
		fmt.Println(mchord.InRange(3, 5, 4))                                                                   //true
		fmt.Println(mchord.InRange(3, 2, 5))                                                                   //true
		fmt.Println(mchord.InRange(3, 5, 7))                                                                   //false
		fmt.Println(mchord.InRange(7, 5, 3))                                                                   //true
		fmt.Println(mchord.InRange(1, 1, 2))                                                                   //true
		fmt.Println(mchord.InRange(Hash("127.0.0.1:12347"), Hash("127.0.0.1:12347"), Hash("127.0.0.1:12348"))) //true
	*/

	ready := make(chan bool)
	n := mchord.Node{
		Addr:  "100.90.243.35:" + os.Args[1],
		Store: store.NewStorage(),
		Ready: ready,
		R:     1,
	}
	n.JoinChordRing("100.90.240.249:12348")
	ready = make(chan bool)
	<-ready
}
