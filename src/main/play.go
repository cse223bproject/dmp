package main

import (
	"fmt"
	"mchord"
	"musicplayer"
	"time"
)

func main() {
	n1 := mchord.Node{
		Addr: "100.90.244.186:12348",
		R:    1,
	}
	lib := musicplayer.Library{
		Base: n1,
	}
	time.Sleep(time.Millisecond * 3000)
	lib.Upload("Azhagiye", "/home/saienthan/Courses/SP17/223b/data/Azhagiye.mp3")
	lib.QueuePlay("Azhagiye", int64(0))
	fmt.Println("Nodes are created")
	//library.Upload("Azhagiye", "/home/saienthan/Courses/SP17/223b/data/Azhagiye.mp3")
}
